using net_core_simple_app_on_gitlab.Services;
using NUnit.Framework;
using System.Linq;

namespace net_core_simple_app_on_gitlab.Tests
{
    [TestFixture]
    public class ValuesServiceTests
    {
        private readonly IValuesService _valuesService;

        public ValuesServiceTests()
        {
            _valuesService = new ValuesService();
        }

        [Test]
        public void GetValues_Return_TwoValues()
        {
            var values = _valuesService.GetValues();
            Assert.AreEqual(values.Length, 2);
        }

        //[Test]
        //public void Inconclusive_Test()
        //{
        //    Assert.Inconclusive();
        //}

        //[Test]
        //public void Failing_Test()
        //{
        //    Assert.Fail();
        //}
    }
}