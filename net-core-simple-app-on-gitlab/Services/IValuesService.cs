﻿using System.Collections.Generic;

namespace net_core_simple_app_on_gitlab.Services
{
    public interface IValuesService
    {
        string[] GetValues();
    }
}