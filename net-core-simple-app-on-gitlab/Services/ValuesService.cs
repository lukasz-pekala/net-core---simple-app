﻿using System.Collections.Generic;

namespace net_core_simple_app_on_gitlab.Services
{
    public class ValuesService : IValuesService
    {
        public string[] GetValues()
            => new[] { "Value1", "Value2" };
    }
}